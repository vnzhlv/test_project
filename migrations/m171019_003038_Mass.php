<?php

use yii\db\Schema;
use yii\db\Migration;

class m171019_003038_Mass extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable('{{%k_ingredient}}',[
            'id_ingredient'=> $this->primaryKey(11),
            'name_ingredient'=> $this->string(128)->notNull(),
            'visible'=> $this->smallInteger(1)->notNull()->defaultValue(1),
        ], $tableOptions);


        $this->createTable('{{%k_dish}}',[
            'id_dish'=> $this->primaryKey(11),
            'name_dish'=> $this->string(128)->notNull(),
            'visible'=> $this->smallInteger(1)->notNull()->defaultValue(1),
        ], $tableOptions);


        $this->createTable('{{%k_ingredient_dish}}',[
            'id_ingredient'=> $this->integer(11)->notNull(),
            'id_dish'=> $this->integer(11)->notNull(),
        ], $tableOptions);

        $this->createIndex('FK_dish','{{%k_ingredient_dish}}',['id_dish'],false);
        $this->addPrimaryKey('pk_on_k_ingredient_dish','{{%k_ingredient_dish}}',['id_ingredient','id_dish']);
        $this->addForeignKey(
            'fk_k_ingredient_dish_id_dish',
            '{{%k_ingredient_dish}}', 'id_dish',
            '{{%k_dish}}', 'id_dish',
            'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'fk_k_ingredient_dish_id_ingredient',
            '{{%k_ingredient_dish}}', 'id_ingredient',
            '{{%k_ingredient}}', 'id_ingredient',
            'RESTRICT', 'CASCADE'
        );
    }

    public function safeDown()
    {
            $this->dropForeignKey('fk_k_ingredient_dish_id_dish', '{{%k_ingredient_dish}}');
            $this->dropForeignKey('fk_k_ingredient_dish_id_ingredient', '{{%k_ingredient_dish}}');
            $this->dropTable('{{%k_ingredient}}');
            $this->dropTable('{{%k_dish}}');
            $this->dropPrimaryKey('pk_on_k_ingredient_dish','{{%k_ingredient_dish}}');
            $this->dropTable('{{%k_ingredient_dish}}');
    }
}
