<?php

namespace app\modules\kitchen;

/**
 * kitchen module definition class
 */
class Kitchen extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\kitchen\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
