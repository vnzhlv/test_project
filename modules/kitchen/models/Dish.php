<?php

namespace app\modules\kitchen\models;

use Yii;

/**
 * This is the model class for table "k_dish".
 *
 * @property integer $id_dish
 * @property string $name_dish
 * @property integer $visible
 *
 * @property IngredientDish[] $IngredientDishes
 * @property Ingredient[] $idIngredients
 */
class Dish extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'k_dish';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_dish'], 'required'],
            [['visible'], 'integer'],
            [['name_dish'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_dish' => 'Id Dish',
            'name_dish' => 'Name of the Dish',
            'visible' => 'Visible status',
            'idIngredients' => 'idIngredients'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientDishes()
    {
        /*hasMany (class, [attribute of IngredientDish => attribute of Dish])*/
        return $this->hasMany(IngredientDish::className(), ['id_dish' => 'id_dish']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id_ingredient' => 'id_ingredient'])->viaTable('k_ingredient_dish', ['id_dish' => 'id_dish']);
    }
}
