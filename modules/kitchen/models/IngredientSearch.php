<?php

namespace app\modules\kitchen\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\kitchen\models\Ingredient;

/**
 * IngredientSearch represents the model behind the search form about `app\modules\kitchen\models\Ingredient`.
 */
class IngredientSearch extends Ingredient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ingredient', 'visible'], 'integer'],
            [['name_ingredient'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ingredient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_ingredient' => $this->id_ingredient,
            'visible' => $this->visible,
        ]);

        $query->andFilterWhere(['like', 'name_ingredient', $this->name_ingredient]);

        return $dataProvider;
    }
}
