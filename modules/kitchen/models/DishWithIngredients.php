<?php

namespace app\modules\kitchen\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "k_dish".
 *
 * @property integer $id_dish
 * @property string $name_dish
 *
 * @property IngredientDish[] $kIngredientDishes
 * @property Ingredient[] $idIngredients
 */
class DishWithIngredients extends Dish
{
    /**
     * @var array IDs of the ingredients
     */
    public $ingredient_ids = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /* https://github.com/yiisoft/yii2/blob/master/docs/guide/tutorial-core-validators.md#yiivalidatorseachvalidatoreach- */
        return ArrayHelper::merge(parent::rules(), [
            ['ingredient_ids', 'each', 'rule' => [
                'exist', 'targetClass' => Ingredient::className(), 'targetAttribute' => 'id_ingredient'
                ],

            ],
            [['ingredient_ids'], 'required']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),[
            'ingredient_ids' => 'Ingredients'
        ]);
    }

    /**
     * load the dishes ingredients
     */
    public function loadIngredients()
    {
        $this->ingredient_ids = [];
        if (!empty($this->id_dish)) {
            $rows = IngredientDish::find()
                ->select(['id_ingredient'])
                ->where(['id_dish' => $this->id_dish])
                ->asArray()
                ->all();
            foreach($rows as $row) {
                $this->ingredient_ids[] = $row['id_ingredient'];
            }
        }
    }

    /**
     * save the dishes ingredients
     */
    public function saveIngredients()
    {
        /* clear the ingredients of the Dish before saving */
        IngredientDish::deleteAll(['id_dish' => $this->id_dish]);
        if (is_array($this->ingredient_ids)) {
            foreach($this->ingredient_ids as $ingredient_id) {
                $ingDish = new IngredientDish();
                $ingDish->id_dish= $this->id_dish;
                $ingDish->id_ingredient = $ingredient_id;
                $ingDish->save();
            }
        }
    }
}
