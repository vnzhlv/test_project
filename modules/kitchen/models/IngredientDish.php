<?php

namespace app\modules\kitchen\models;

use Yii;

/**
 * This is the model class for table "k_ingredient_dish".
 *
 * @property integer $id_ingredient
 * @property integer $id_dish
 *
 * @property Dish $idDish
 * @property Ingredient $idIngredient
 */
class IngredientDish extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'k_ingredient_dish';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ingredient', 'id_dish'], 'required'],
            [['id_ingredient', 'id_dish'], 'integer'],
            [['id_dish'], 'exist', 'skipOnError' => true, 'targetClass' => Dish::className(), 'targetAttribute' => ['id_dish' => 'id_dish']],
            [['id_ingredient'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['id_ingredient' => 'id_ingredient']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ingredient' => 'Id Ingredient',
            'id_dish' => 'Id Dish'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDish()
    {
        return $this->hasOne(Dish::className(), ['id_dish' => 'id_dish']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id_ingredient' => 'id_ingredient']);
    }
}
