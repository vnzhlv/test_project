<?php

namespace app\modules\kitchen\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "k_ingredient".
 *
 * @property integer $id_ingredient
 * @property string $name_ingredient
 * @property integer $visible
 *
 * @property IngredientDish[] $ingredientDishes
 * @property Dish[] $idDishes
 */
class Ingredient extends \yii\db\ActiveRecord
{
    const STATUS_VISIBLE =1;
    const STATUS_HIDDEN = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'k_ingredient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ingredient'], 'required'],
            [['visible'], 'integer'],
            [['name_ingredient'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ingredient' => 'Id Ingredient',
            'name_ingredient' => 'Name of the Ingredient',
            'visible' => 'Visible status',
        ];
    }

    /**
     * Get all the ingredients with visible status = 1.
     * @return array available ingredients
     */
    public static function getAvailableIngredients()
    {
        $ingredients = self::find()->where(['visible' => '1'])->orderBy('name_ingredient')->asArray()->all();
        $items = ArrayHelper::map($ingredients, 'id_ingredient', 'name_ingredient');
        return $items;
    }

    public static function getVisibleStatus()
    {
        return [
            self::STATUS_VISIBLE => 'Visible',
            self::STATUS_HIDDEN => 'Hidden'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientDishes()
    {
        return $this->hasMany(IngredientDish::className(), ['id_ingredient' => 'id_ingredient']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdDishes()
    {
        return $this->hasMany(Dish::className(), ['id_dish' => 'id_dish'])->viaTable('k_ingredient_dish', ['id_ingredient' => 'id_ingredient']);
    }
}
