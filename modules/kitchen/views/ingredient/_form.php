<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\kitchen\models\Ingredient */
/* @var $form yii\widgets\ActiveForm */
//$model->visible = 1;
//var_dump($model->visible);
/*if ($model->visible === NULL)
    $model->visible = 1;*/

?>

<div class="ingredient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_ingredient')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'visible')->radioList($model::getVisibleStatus()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
