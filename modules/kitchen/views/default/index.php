<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\kitchen\models\Dish */
/* @var $form yii\widgets\ActiveForm */
/* @var $ingredients ... */
?>

<div class="dish-form">

    <?php $form = ActiveForm::begin([
            'action' => '/kitchen/default/search',

    ]); ?>


    <?= $form->field($model, 'id_ingredient')
        /*->listBox($ingredients, ['multiple' => true])*/
        ->checkboxList($ingredients)
        ->label('Ingredients')
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search dishes', [
            'class'=>'btn btn-primary'
        ]) ?>

    </div>

    <?php ActiveForm::end(); ?>
