<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\kitchen\models\Dish;
/* @var $msg ... */
$this->title = 'Search';
$this->params['breadcrumbs'][] = ['label' => 'Kitchen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Search'];
print_r($data);
?>
<div class="search">

    <?php
    if (!empty($data)) {
        foreach ($data as $item) {
            echo "<b>" . $item['name_dish'] ."</b>" . ", количество совпадений игредиентов: " . $item['matches'] . "<BR>";
        }
    }
    else
        echo $msg;

    ?>

</div>
