<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\kitchen\models\Dish;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\kitchen\models\DishSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dishes';
$this->params['breadcrumbs'][] = $this->title;
$dish = Dish::findOne(7);
/*echo "<pre>"; print_r($dish->idIngredients[1]->name_ingredient);  echo "</pre>";*/
use yii\bootstrap\Alert;
?>
<div class="dish-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Dish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*'id_dish',*/
            'name_dish',
            'visible',
            /*[
                //'attribute' => 'id_dish',
                //'format' => '',
                //'label' => '',
                //'visible' => '',
            ],*/

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
