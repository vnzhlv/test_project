<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\kitchen\models\Dish */

$this->title = $model->id_dish;
$this->params['breadcrumbs'][] = ['label' => 'Dishes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dish-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_dish], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_dish], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_dish',
            'name_dish',
            'visible',
            [
                'label'=> 'Ingredients',
                'format' => 'html',
                'value' => function ($model){
                    $item = '';
                    foreach ($model->idIngredients as $key => $idIngredient) {
                        $item .= $key+1 . ") ".$idIngredient->name_ingredient . " <br>";
                    }
                    return $item;
                },
            ]
        ],
    ]) ?>

</div>
