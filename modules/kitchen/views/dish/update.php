<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\kitchen\models\Dish */
/* @var $ingredients app\modules\kitchen\models\Dish */

$this->title = 'Update Dish: ' . $model->id_dish;
$this->params['breadcrumbs'][] = ['label' => 'Dishes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_dish, 'url' => ['view', 'id' => $model->id_dish]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dish-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ingredients' => $ingredients
    ]) ?>

</div>
