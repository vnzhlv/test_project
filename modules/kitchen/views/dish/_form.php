<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\kitchen\models\Dish */
/* @var $form yii\widgets\ActiveForm */
/* @var $ingredients ... */
?>

<div class="dish-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_dish')->textInput(); ?>

    <?= $form->field($model, 'ingredient_ids')
        ->listBox($ingredients, ['multiple' => true])
        /* ->checkboxList($ingredients) */
        ->hint('Select the ingredients');
    ?>

    <div class="form-group">
        <?= Html::submitButton('Create', [
            'class' => 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


    <?php /*$form = ActiveForm::begin(); */?><!--

    <?/*= $form->field($model, 'name_dish')->textInput(['maxlength' => true]) */?>

    <div class="form-group">
        <?/*= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) */?>
    </div>

    --><?php /*ActiveForm::end(); */?>