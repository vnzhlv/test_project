<?php

namespace app\modules\kitchen\controllers;

use app\modules\kitchen\models\Dish;
use app\modules\kitchen\models\Ingredient;
use app\modules\kitchen\models\DishWithIngredients;
use app\modules\kitchen\models\IngredientDish;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `kitchen` module
 */
class DefaultController extends Controller
{
    /**
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Ingredient();
        $ingredients = Ingredient::getAvailableIngredients();
        return $this->render('index', [
            'model' => $model,
            'ingredients' => $ingredients
        ]);
    }

    public function actionSearch()
    {
        $post = Yii::$app->request->post('Ingredient')['id_ingredient'];
        $data = $this->validateData($post);

        return $this->render('search', [
            'data' => $data['data'],
            'msg' => $data['msg']
        ]);
    }

    protected function validateData($post)
    {

        if (count($post) < 2 || count($post) > 5) {
            return [
                'data' => '',
                'msg' => 'Выберите от 2 до 5 ингредиентов'
            ];
        }

        $choosedIngredients = implode(', ', $post);

        $sql = "
                    SELECT 
                            d.name_dish, COUNT(di.id_ingredient) AS matches 
                    FROM 
                            k_dish d
                    JOIN 
                            k_ingredient_dish di
                    ON 
                            d.id_dish=di.id_dish
                    WHERE
                            di.id_ingredient IN (".$choosedIngredients.")
                    GROUP BY
                            d.name_dish
                    HAVING 
                            COUNT(di.id_ingredient) > 1
                    ORDER BY 
                            matches DESC;
                ";

        $dishes = Yii::$app->db->createCommand($sql)
            //->bindValue(':choosedIngredients', $choosedIngredients)
            ->queryAll();

        if (empty($dishes)) {
            return [
                'data' => '',
                'msg' => 'Ничего не найдено'
            ];
        }

        return [
            'data' => $dishes,
            'msg' => 'Успешно'
        ];
    }
}
