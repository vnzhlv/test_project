# Установка #

Миграции:
yii migrate --migrationPath=@yii/rbac/migrations
yii migrate для основных таблиц и для таблиц авторизации.

# Доступные действия #
Админская часть:
/kitchen/dish
/kitchen/ingredient

Пользовательская часть:
/kitchen/

Список админов в модуле 'user' в config\web.php 